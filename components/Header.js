import React,{useState}from 'react';
import {View, Text, StyleSheet, TouchableOpacity,TextInput} from 'react-native';
import { Colors } from 'react-native/Libraries/NewAppScreen';

const Header = (props) => { 
    const [input, setInput] = useState('');  
    
    return (
        <View>
            <Text style={styles.head}>{props.title}</Text>
            <View style={styles.container}>
                <TextInput
                    style={styles.input}
                    value={input}
                    
                    onChangeText={(text) => setInput(text)}/>
                <TouchableOpacity style={styles.button} onPress={() => props.searchUsers(input)}>
                    <Text style={styles.serch}>Buscar</Text>
                </TouchableOpacity>
            </View>

        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        width: '100%',
        backgroundColor:'#3c7473',
      },
      input: {
        borderWidth: 1,
        borderColor: '#5AC18E',
        width: '80%',
        height: 40,
        borderRadius: 15,
        margin: 5,
        color:Colors.lighter,
        fontSize:18,
      },
      button: {
        justifyContent: 'center',
        color:'gray',
      },
      serch:{
        color:'white',
      },
    
    head:{
        textAlign:'center',
        textAlignVertical:'center',
        backgroundColor:'#365B5C',
        height: 30,
        fontWeight: 'bold',
        fontSize: 15,
        color:'white',
    },
  header: {
    width: '100%',
    height: 60,
    flexDirection: 'row',
    backgroundColor: 'lightblue',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 20,
  },
  title: {
    color: '#fff',
    fontSize: 20,
    fontWeight: 'bold',
  },
  appButton: {
    backgroundColor: 'blue',
    height: 60,
    width: 160,
    justifyContent: 'center',
    alignItems: 'center',
  },
  appTitle: {
    color: '#fff',
  },
});

export default Header;
