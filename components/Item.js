import React from 'react';
import {View, Text, Image, StyleSheet, SafeAreaView} from 'react-native';
import { Colors } from 'react-native/Libraries/NewAppScreen';

const Item=(props)=>{   
    const {name,birthday,img,status,nickname}=props.item;
    return(        
            <View style={styles.item}>            
                <View style={styles.contenImage}>               
                        <Image
                            style={styles.image}
                            source={{
                                uri:img
                            }}
                        />
                </View>                    
                <View style={styles.contenido}>
                    <Text style={styles.name}>{name}</Text>    
                    <Text style={styles.birthday}>Fecha de Nacimiento: {birthday=='Unknown'?('Desconocido'):(birthday)}</Text>
                    <Text style={styles.state}>Estado: {status}</Text>
                    <Text style={styles.nick}>Apodo: {nickname}</Text> 
                    {/* <View style={{flexDirection:'row'}}>
                        <Button title={'Editar'} onPress={() => console.log(id)} />
                        <Button title={'Eliminar'} onPress={() => console.log(id)} />
                    </View>                         */}
                </View>
                


            </View>       
        
    );
};
const styles =StyleSheet.create({
    nick:{        
        margin:25,
        fontWeight:'bold',
        color:'orange',
    },
    state:{
        color:'white',
        marginLeft:10,
    },
    birthday:{
        fontSize:15,
        color:'gray',
        marginLeft:10,
    },
    name:{
        marginLeft:20,
        fontWeight:'bold',
        fontSize:20,
        color:Colors.lighter,
    },
    item:{
        flexDirection:'row',
        borderWidth:1,
        backgroundColor:Colors.dark,
        padding:5,
        height:150,
        
    },
    contenImage:{
        flex:1,
    },
    image:{        
        width: 120,
        height: '100%',
    },
    contenido:{
        flex:2,
        padding:10,
    }
});
export default Item ;