import React, { useState ,useEffect,useMemo} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ActivityIndicator
} from 'react-native';
import axios from 'axios';
//import Item from './components/Item';
import List from './components/List';
import Header from './components/Header';


import { LearnMoreLinks, Colors, DebugInstructions, ReloadInstructions,} from 'react-native/Libraries/NewAppScreen';



const App= () => {
  const [Text,setText]=useState('');
  const [conten,setConten]=useState(null);

  const updateConte=(value)=>{
    setConten([...conten,value])
  }
  const fetchData = () => {
    axios
      .get('https://breakingbadapi.com/api/characters')
      .then((response) => setConten(response.data)
      //,console.log('-_--_'+conten)
      )
      .catch((err) => console.log(err));
  }

  useEffect(() => fetchData(),[]);  


  const searchUsers = (Text) => {
    if (!conten) {
      return null;
    }
    console.log(Text);
    return conten.filter((user) => user.name.toLowerCase().includes(Text));
  };
  const filteredUsers = useMemo(() => searchUsers(Text), [conten, Text]);


  return (
    <View style={styles.body}>   
      <Header title={'PERSONJES'} searchUsers={setText}/>   
      {/* <ScrollView> */}
            {filteredUsers ? (
              <List items={filteredUsers} />   ) : (   
              <ActivityIndicator            
                size={50} 
                color={'gray'}
              />
          )}  
        
      {/* </ScrollView>        */}

    </View>
  );
};

const styles = StyleSheet.create({
  
  scrollView: {
    margin:20,
    height:'85%',
    backgroundColor:'#607D8B',
    borderRadius:5,
    borderColor:Colors.dark,
    borderWidth:1,
    padding:2,
  },
  body:{
    height:'100%',
    backgroundColor:'#365b5c',
  }
});

export default App;
